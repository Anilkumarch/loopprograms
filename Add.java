package loops;

import java.util.Scanner;

//created class name as Add
class Add {
	// public is a access specifier
	// static is a class
	// main method
	public static void main(String args[]) {

		// variable initialisation
		int Firstvalue, Secondvalue, Addition;
		// print statement
		System.out.println("Enter two numbers to calculate their sum");

		// Scanner class without initiation of data type
		Scanner in = new Scanner(System.in);
		//object for scanner
		Firstvalue = in.nextInt();
		Secondvalue = in.nextInt();
		Addition = Firstvalue + Secondvalue;
		// prints output
		System.out.println("Sum of the numbers = " + Addition);
	}
}