package loops;

import java.util.Scanner;

public class WhileEven {

	private static Scanner sc;
	public static void main(String[] args) 
	{
		int number, i;
		sc = new Scanner(System.in);
		
		System.out.print(" Please Enter any Number : ");
		number = sc.nextInt();	
		i = 2; 
		
		while(i <= number)
		{
			System.out.print(i +"\t"); 
			i = i + 2;
		}	
	}
}